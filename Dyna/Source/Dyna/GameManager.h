// Made by Arendariuk Andrii

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameManager.generated.h"

UENUM(BlueprintType)
enum class EPickupType : uint8 {
	BlastSize, BombCount, Speed, RemoteControl
};

UCLASS()
class DYNA_API AGameManager : public AActor
{
	GENERATED_BODY()
	
public:	
	AGameManager();

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, Category = "Map|Chunk")
	bool GenerateChunk(FIntVector Pos);

	UFUNCTION(BlueprintCallable, Category = "Map|Chunk")
	bool GenerateChunksAround(FIntVector Pos, int Radius);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Map|Position")
	FIntVector ConvertPositionWorldToCell(FVector Pos);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Map|Position")
	FVector ConvertPositionCellToWorld(FIntVector Pos);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Map|Position")
	FIntVector ConvertPositionCellToChunk(FIntVector Pos);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Map|Position")
	FVector GetChunkPosition(FIntVector Pos);

	UFUNCTION(BlueprintCallable, Category = "Players")
	void SpawnPlayers();

	UFUNCTION(BlueprintCallable, Category = "Pickups")
	bool SpawnPickup(FIntVector Cell, EPickupType Type);

	UFUNCTION(BlueprintCallable, Category = "Search")
	FIntVector FindClosestEmptyCell(FIntVector Cell);

	UFUNCTION(BlueprintCallable, Category = "Conditions")
	bool CheckWinCondition();

	UFUNCTION(BlueprintCallable, Category = "Events")
	void OnRestart();


public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Main)
	TArray<FLinearColor> PlayerColors;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Main)
	TSubclassOf<class ABomberman> PlayerClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Main)
	TSubclassOf<class AEnemy> EnemyClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Main)
	TSubclassOf<class ABomb> BombClass;


	// Widgets
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Widgets)
	TSubclassOf<class UScoreWidget> ScoreWidgetClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Widgets)
	TSubclassOf<class UGameOverWidget> GameOverWidgetClass;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Widgets)
	class UScoreWidget* ScoreWidget;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Widgets)
	class UGameOverWidget* GameOverWidget = nullptr;
	// ---


	// Pickups
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Pickups)
	TMap<EPickupType, float> PickupSpawnChance;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Pickups)
	TSubclassOf<class APickup> PickupBlastSizeClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Pickups)
	TSubclassOf<class APickup> PickupBombCountClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Pickups)
	TSubclassOf<class APickup> PickupSpeedClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Pickups)
	TSubclassOf<class APickup> PickupRemoteControlClass;
	// --- 


	// For world generation
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Pickups)
	float StoneSpawnChance = 60.0f;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = WorldGen)
	TSubclassOf<AActor> GroundClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = WorldGen)
	TSubclassOf<class AWall> WallClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = WorldGen)
	TSubclassOf<class AStone> StoneClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = WorldGen)
	FVector ChunkSize = {1000, 1000, 0};

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = WorldGen)
	FIntVector ChunkCapacity = { 10, 10, 0 }; // how many cells in a chunk 

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = WorldGen)
	FVector CellSize = { 100, 100, 0 };

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = WorldGen)
	int ViewRange = 3000;
	// ---


	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Map)
	TSet<FIntVector> Chunks;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Map)
	TMap<FIntVector, class AWall*> Map;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Map)
	TMap<FIntVector, class APickup*> Pickups;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Map)
	TMap<FIntVector, class ABomb*> Bombs;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Map)
	TArray<class ABomberman*> Players;
};
