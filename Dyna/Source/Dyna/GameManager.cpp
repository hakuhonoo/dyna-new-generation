// Made by Arendariuk Andrii

#include "GameManager.h"
#include "EngineMinimal.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

#include "Stone.h"
#include "Bomberman.h"
#include "Pickup.h"
#include "ScoreWidget.h"
#include "GameOverWidget.h"
#include "Bomb.h"


FIntVector operator*(FIntVector obj, const FIntVector& other) {
	return FIntVector(obj.X * other.X, obj.Y * other.Y, obj.Z * other.Z);
}

FVector operator*(FIntVector obj, const FVector& other) {
	return FVector(obj.X * other.X, obj.Y * other.Y, obj.Z * other.Z);
}

AGameManager::AGameManager()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AGameManager::BeginPlay()
{
	Super::BeginPlay();

	if (ScoreWidgetClass) {
		ScoreWidget = CreateWidget<UScoreWidget>(GetWorld(), ScoreWidgetClass);
		if (!ScoreWidget) {
			UE_LOG(LogTemp, Error, TEXT("Cannot create score widget"));
			return;
		}
	}
	ScoreWidget->AddToViewport();

	GenerateChunksAround(FIntVector::ZeroValue, ViewRange);
	SpawnPlayers();
}

bool AGameManager::GenerateChunk(FIntVector Pos)
{
	if (Chunks.Contains(Pos)) {
		return false;
	}

	// Spawn ground
	auto Location = GetChunkPosition(Pos);
	auto Ground = GetWorld()->SpawnActor(GroundClass, &Location);
	if (!Ground) {
		UE_LOG(LogTemp, Error, TEXT("Cannot spawn chunk ground"));
		return false;
	}

	// Spawn walls in chess-like style & stones with pickups
	auto ChunkCellPosOrigin = Pos * ChunkCapacity;
	for (int i = 0; i < ChunkCapacity.X; i++) {
		for (int j = 0; j < ChunkCapacity.Y; j++) {
			auto AbsoluteCellPos = ChunkCellPosOrigin + FIntVector(i, j, 0);
			if ((FMath::Abs(AbsoluteCellPos.X % 2) == 1) && (FMath::Abs(AbsoluteCellPos.Y % 2) == 1)) { // place for a wall
				Location = ConvertPositionCellToWorld(AbsoluteCellPos);
				auto Wall = GetWorld()->SpawnActor(WallClass, &Location);
				if (!Wall) {
					UE_LOG(LogTemp, Warning, TEXT("Cannot spawn wall on the chunk"));
				}
				else {
					Map.Add(AbsoluteCellPos, Cast<AWall>(Wall));
				}
			}
			else { // place for a stone

				// stone spawn chance
				float Chance = FMath::RandRange(0.0f, 100.0f);
				if (Chance <= StoneSpawnChance) { 
					Location = ConvertPositionCellToWorld(AbsoluteCellPos);
					auto Stone = Cast<AStone>(GetWorld()->SpawnActor(StoneClass, &Location));
					if (!Stone) {
						UE_LOG(LogTemp, Warning, TEXT("Cannot spawn stone on the chunk"));
					}
					else {
						Map.Add(AbsoluteCellPos, Stone);
					}

					// pickup spawn complex chance
					Chance = FMath::RandRange(0.0f, 100.0f);
					float Min = 0;
					float Max;
					for (auto Item : PickupSpawnChance) {
						Max = Min + Item.Value;
						if (Chance >= Min && Chance < Max) {
							Stone->IsPickupHidden = true;
							Stone->PickupType = Item.Key;
							break;
						}
						Min = Max;
					}
				}
			}
		}
	}

	Chunks.Add(Pos);
	UE_LOG(LogTemp, Log, TEXT("Chunk %s generated successfully"), *Pos.ToString());
	return true;
}

bool AGameManager::GenerateChunksAround(FIntVector Pos, int Radius)
{
	int Range = Radius / FMath::Min(ChunkSize.X, ChunkSize.Y);
	bool Result = false;

	for (int i = -Range; i <= Range; i++) {
		for (int j = -Range; j <= Range; j++) {
			Result = GenerateChunk(Pos + FIntVector(i, j, 0)) || Result;
		}
	}
	return Result;
}

FIntVector AGameManager::ConvertPositionWorldToCell(FVector Pos)
{
	return FIntVector(FMath::FloorToInt(Pos.X / CellSize.X), FMath::FloorToInt(Pos.Y / CellSize.Y), 0);
}

FVector AGameManager::ConvertPositionCellToWorld(FIntVector Pos)
{
	return Pos * CellSize;
}

FIntVector AGameManager::ConvertPositionCellToChunk(FIntVector Pos)
{
	return FIntVector(Pos.X / ChunkCapacity.X, Pos.Y / ChunkCapacity.Y, 0);
}

FVector AGameManager::GetChunkPosition(FIntVector Pos)
{
	return Pos * ChunkSize;
}

void AGameManager::SpawnPlayers()
{
	if (PlayerColors.Num() < 2) {
		UE_LOG(LogTemp, Error, TEXT("AGameManager::PlayerColors is too small"));
		return;
	}

	auto ClearSurroundingArea = [this](FIntVector Cell) {
		TSet<FIntVector> Cells = { 
			Cell + FIntVector{ -1, 0, 0 },  
			Cell + FIntVector{ 1, 0, 0 },
			Cell + FIntVector{ 0, -1, 0 },
			Cell + FIntVector{ 0, 1, 0 },
			Cell + FIntVector{ -1, -1, 0 },
			Cell + FIntVector{ -1, 1, 0 },
			Cell + FIntVector{ 1, -1, 0 },
			Cell + FIntVector{ 1, 1, 0 },
		};
		for (auto Item : Cells) {
			if (Map.Contains(Item) && Map[Item]->IsDestructible) {
				Map[Item]->Destroy();
				Map.Remove(Item);
			}
		}
	};

	// First player
	auto Cell = FindClosestEmptyCell({ 0, 0, 0 });
	auto Location = ConvertPositionCellToWorld(Cell) + CellSize / 2;
	auto Player = Cast<ABomberman>(GetWorld()->SpawnActor(PlayerClass, &Location));
	if (!Player) {
		UE_LOG(LogTemp, Error, TEXT("AGameManager::SpawnPlayers: Cannot spawn player 1"));
		return;
	}
	Player->InitPlayer(0, PlayerColors[0]);
	Player->Cell = Cell;
	Players.Add(Player);
	ClearSurroundingArea(Cell);

	// Second player
	Cell = FindClosestEmptyCell({ -5, -5, 0 });
	Location = ConvertPositionCellToWorld(Cell) + CellSize / 2;
	Player = Cast<ABomberman>(GetWorld()->SpawnActor(PlayerClass, &Location));
	if (!Player) {
		UE_LOG(LogTemp, Error, TEXT("AGameManager::SpawnPlayers: Cannot spawn player 2"));
		return;
	}
	Player->InitPlayer(1, PlayerColors[1]);
	Player->Cell = Cell;
	Players.Add(Player);
	ClearSurroundingArea(Cell);
}

bool AGameManager::SpawnPickup(FIntVector Cell, EPickupType Type)
{
	if (Pickups.Contains(Cell)) {
		UE_LOG(LogTemp, Error, TEXT("AGameManager::SpawnPickup: Can't spawn a pickup, place is occupied"));
		return false;
	}

	UClass* PickupClass;
	switch (Type) {
	case EPickupType::BlastSize:
		PickupClass = PickupBlastSizeClass;
		break;
	case EPickupType::BombCount:
		PickupClass = PickupBombCountClass;
		break;
	case EPickupType::RemoteControl:
		PickupClass = PickupRemoteControlClass;
		break;
	case EPickupType::Speed:
		PickupClass = PickupSpeedClass;
		break;
	default:
		UE_LOG(LogTemp, Error, TEXT("AGameManager::SpawnPickup: Unknown pickup type"));
		return false;
	}

	auto Location = ConvertPositionCellToWorld(Cell) + CellSize / 2;
	auto Pickup = Cast<APickup>(GetWorld()->SpawnActor(PickupClass, &Location));
	if (!Pickup) {
		UE_LOG(LogTemp, Error, TEXT("AGameManager::SpawnPickup: Cannot spawn puckup"));
		return false;
	}

	Pickups.Add(Cell, Pickup);
	return true;
}

FIntVector AGameManager::FindClosestEmptyCell(FIntVector Cell)
{
	// TODO: Make spiral iteration
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			auto AbsoluteCellPos = Cell + FIntVector(i, j, 0);

			// Skip wall pos
			if ((AbsoluteCellPos.X % 2 == 1) && (AbsoluteCellPos.Y % 2 == 1)) {
				continue;
			}

			if (!Map.Contains(AbsoluteCellPos) && !Pickups.Contains(AbsoluteCellPos) && !Bombs.Contains(AbsoluteCellPos)) {
				bool IsTherePlayer = false;
				for (auto Player : Players) {
					if (Player->Cell == AbsoluteCellPos) {
						IsTherePlayer = true;
						break;
					}
				}

				if (!IsTherePlayer) {
					return AbsoluteCellPos;
				}
			}
		}
	}

	return FIntVector();
}

bool AGameManager::CheckWinCondition()
{
	if (Players.Num() == 2) {
		return false;
	}

	if (GameOverWidgetClass) {
		GameOverWidget = CreateWidget<UGameOverWidget>(GetWorld(), GameOverWidgetClass);
		if (!GameOverWidget) {
			UE_LOG(LogTemp, Error, TEXT("Cannot create game over widget"));
			return true;
		}
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Cannot create game over widget, class not set"));
		return true;
	}

	if (Players.Num() == 1) {
		GameOverWidget->Win(Players.Top()->GetPlayerId());
	}
	else {
		GameOverWidget->Draw();
	}
	GameOverWidget->AddToViewport(99);

	APlayerController* MyController = GetWorld()->GetFirstPlayerController();
	UWidgetBlueprintLibrary::SetInputMode_UIOnlyEx(MyController, GameOverWidget);
	MyController->bShowMouseCursor = true;

	return true;
}

void AGameManager::OnRestart()
{
	TMap<int, int> Scores;

	// Clear all things
	for (auto Item : Players) {
		Scores.Add(Item->GetPlayerId(), Item->Score);
		Item->Destroy();
	}
	Players.Empty();

	for (auto Item : Map) {
		Item.Value->Destroy();
	}
	Map.Empty();

	for (auto Item : Bombs) {
		Item.Value->Destroy();
	}
	Bombs.Empty();

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), GroundClass, FoundActors);
	for (auto Item : FoundActors) {
		Item->Destroy();
	}
	Chunks.Empty();

	for (auto Item : Pickups) {
		Item.Value->Destroy();
	}
	Pickups.Empty();


	// Remove game over widget and reset input mode
	if (GameOverWidget) {
		GameOverWidget->RemoveFromViewport();
		GameOverWidget = nullptr;
	}
	
	APlayerController* MyController = GetWorld()->GetFirstPlayerController();
	UWidgetBlueprintLibrary::SetInputMode_GameOnly(MyController);
	MyController->bShowMouseCursor = false;


	// Respawn everything
	GenerateChunksAround(FIntVector::ZeroValue, ViewRange);
	SpawnPlayers();


	// Restore scores for winner (not for looser)
	for (auto Item : Players) {
		if (Scores.Contains(Item->GetPlayerId())) {
			Item->Score = Scores[Item->GetPlayerId()];
			Item->UpdateWidget();
		}
	}
}

