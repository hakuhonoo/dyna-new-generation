// Made by Arendariuk Andrii

#include "Bomb.h"
#include "EngineMinimal.h"
#include "GameManager.h"
#include "Wall.h"
#include "Bomberman.h"


ABomb::ABomb()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ABomb::BeginPlay()
{
	Super::BeginPlay();
	
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AGameManager::StaticClass(), FoundActors);
	if (FoundActors.Num() != 1) {
		UE_LOG(LogTemp, Error, TEXT("ABomb::BeginPlay: Cannot find GameManager"));
		return;
	}
	GameManager = Cast<AGameManager>(FoundActors[0]);
}

void ABomb::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void ABomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABomb::DestroySurroundings()
{
	bool IsSomeoneDie = false;

	auto ExplodeCell = [this, &IsSomeoneDie](FIntVector Cell) {
		if (GameManager->Map.Contains(Cell)) {
			if (GameManager->Map[Cell]->IsDestructible) {
				GameManager->Map[Cell]->Explode();
				GameManager->Map.Remove(Cell);

				BombOwner->Score++;
			}
			return false; // Do not pass through
		}
		if (GameManager->Bombs.Contains(Cell) && GameManager->Bombs[Cell] != this) {

			// Chain explosion create
			if (!ExplosionChainLength.IsValid()) {
				ExplosionChainLength = MakeShared<int>(1);
			}
			else {
				(*ExplosionChainLength)++;
			}

			GameManager->Bombs[Cell]->ExplosionChainLength = ExplosionChainLength;
			GameManager->Bombs[Cell]->Trigger();
		}

		// Kill players
		TArray<ABomberman*> DieList;
		for (auto Player : GameManager->Players) {
			if (Player->Cell == Cell) {
				DieList.Add(Player);
			}
		}
		for (auto Player : DieList) {
			Player->Die();
		}
		IsSomeoneDie = DieList.Num() > 0;
		return true; // Pass through
	};

	ExplodeCell(Cell);

	for (int i = 1; i <= ExplosionRange; i++) {
		if (!ExplodeCell(Cell + FIntVector(i, 0, 0))) {
			break;
		}
	}
	for (int i = -1; i >= -ExplosionRange; i--) {
		if (!ExplodeCell(Cell + FIntVector(i, 0, 0))) {
			break;
		}
	}
	for (int i = 1; i <= ExplosionRange; i++) {
		if (!ExplodeCell(Cell + FIntVector(0, i, 0))) {
			break;
		}
	}
	for (int i = -1; i >= -ExplosionRange; i--) {
		if (!ExplodeCell(Cell + FIntVector(0, i, 0))) {
			break;
		}
	}

	// Chain explosion process
	if (ExplosionChainLength.IsValid()) {
		(*ExplosionChainLength)--;

		if (*ExplosionChainLength == 0) {
			ExplosionChainLength.Reset();

			GameManager->CheckWinCondition();
			BombOwner->UpdateWidget();
		}
	}
	else if (IsSomeoneDie) { // or someone die without chain explosion
		GameManager->CheckWinCondition();
	}
	else {
		BombOwner->UpdateWidget();
	}
}

void ABomb::OnExploded()
{
	BombOwner->BombExploded(this);
}

