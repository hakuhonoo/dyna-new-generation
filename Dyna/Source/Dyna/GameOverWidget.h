// Made by Arendariuk Andrii

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverWidget.generated.h"

/**
 * 
 */
UCLASS()
class DYNA_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widget)
	void Win(int PlayerId);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widget)
	void Draw();
};
