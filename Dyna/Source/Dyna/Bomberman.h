// Made by Arendariuk Andrii

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Bomberman.generated.h"

UCLASS()
class DYNA_API ABomberman : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABomberman();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	UFUNCTION(BlueprintNativeEvent, Category = "Players")
	void InitPlayerExternal();

public:
	UFUNCTION(BlueprintCallable, Category = "Players")
	void InitPlayer(int Id, FLinearColor Color);

	UFUNCTION(BlueprintCallable, Category = "Players")
	int GetPlayerId();

	UFUNCTION(BlueprintCallable, Category = "Actions|Movement")
	void MoveUp(bool Start);

	UFUNCTION(BlueprintCallable, Category = "Actions|Movement")
	void MoveDown(bool Start);

	UFUNCTION(BlueprintCallable, Category = "Actions|Movement")
	void MoveLeft(bool Start);

	UFUNCTION(BlueprintCallable, Category = "Actions|Movement")
	void MoveRight(bool Start);

	UFUNCTION(BlueprintCallable, Category = "Actions")
	void PutBomb();

	UFUNCTION(BlueprintCallable, Category = "Actions")
	void Trigger();

	UFUNCTION(BlueprintCallable, Category = "Events")
	void Die();

	UFUNCTION(BlueprintCallable, Category = "Events")
	void BombExploded(class ABomb* Bomb);

	UFUNCTION(BlueprintCallable, Category = "Events")
	void UpdateWidget();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Events")
	void OnFrontSideUpdate(FVector Direction);

protected:
	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Player)
	int PlayerId = 0;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Player)
	bool IsMovingUp = false;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Player)
	bool IsMovingDown = false;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Player)
	bool IsMovingLeft = false;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Player)
	bool IsMovingRight = false;

public:
	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = System)
	class AGameManager* GameManager = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = Player)
	FLinearColor PlayerColor;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Player)
	float NormalSpeed = 150;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Player)
	float CurrentSpeed;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Player)
	int Score = 0;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Player)
	FIntVector Cell;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = BombRemote)
	bool HasBombRemote = false;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = BombRemote)
	bool RemoteBombPlaced = false;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = BombRemote)
	float BombRemoteDuration = 10; // sec

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = BombRemote)
	float CurrentBombRemoteDuration = 0; // sec

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Bomb)
	TArray<class ABomb*> Bombs;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Bomb)
	int BombCount = 1;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Bomb)
	int ExplosionRange = 1;
};
