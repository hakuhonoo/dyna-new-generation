// Made by Arendariuk Andrii

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Wall.generated.h"

UCLASS()
class DYNA_API AWall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Wall")
	void Explode();

public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Wall")
	bool IsDestructible = false;
};
