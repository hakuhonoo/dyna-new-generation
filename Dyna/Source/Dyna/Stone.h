// Made by Arendariuk Andrii

#pragma once

#include "CoreMinimal.h"
#include "Wall.h"
#include "GameManager.h"
#include "Stone.generated.h"

/**
 * 
 */
UCLASS()
class DYNA_API AStone : public AWall
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;


public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stone")
	bool IsPickupHidden = false;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stone")
	EPickupType PickupType;
};
