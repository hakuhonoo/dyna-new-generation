// Made by Arendariuk Andrii

#include "Bomberman.h"
#include "EngineMinimal.h"
#include "GameManager.h"
#include "Bomb.h"
#include "ScoreWidget.h"
#include "Pickup.h"


ABomberman::ABomberman()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABomberman::BeginPlay()
{
	Super::BeginPlay();
	
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AGameManager::StaticClass(), FoundActors);
	if (FoundActors.Num() != 1) {
		UE_LOG(LogTemp, Error, TEXT("ABomberman::BeginPlay: Cannot find GameManager"));
		return;
	}
	GameManager = Cast<AGameManager>(FoundActors[0]);

	// Some init
	CurrentSpeed = NormalSpeed;
}

void ABomberman::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector Pos = GetActorLocation();
	FVector Shift = FVector::ZeroVector;
	FVector SecondaryShift = FVector::ZeroVector; // used if primary not triggered by player for movement smoothing between walls
	FVector LocalPos = Pos - GameManager->ConvertPositionCellToWorld(Cell);
	FVector CellSize = GameManager->CellSize;

	if (IsMovingUp) {
		if (GameManager->Map.Contains(Cell + FIntVector(0, 1, 0))) { // if there is a wall - try to bypass it from a side
			if (LocalPos.Y < CellSize.Y / 2) { // if we can go little bit more to center
				SecondaryShift.Y = 1;
			}
			else { // can't move to this direction any more
				if (LocalPos.X < CellSize.X / 2) {
					SecondaryShift.X = !GameManager->Map.Contains(Cell + FIntVector(-1, 1, 0)) && !GameManager->Map.Contains(Cell + FIntVector(-1, 0, 0)) ? -1 : 0;
				}
				else {
					SecondaryShift.X = !GameManager->Map.Contains(Cell + FIntVector(1, 1, 0)) && !GameManager->Map.Contains(Cell + FIntVector(1, 0, 0)) ? 1 : 0;
				}
			}
		}
		else { // no wall
			if (FMath::IsNearlyEqual(LocalPos.X, CellSize.X / 2, CellSize.X / 10)) { // if we on center of the cell
				Shift.Y = 1;
			}
			else { // need to be centred
				SecondaryShift.X = LocalPos.X < CellSize.X / 2 ? 1 : -1;
			}
		}
	}
	if (IsMovingDown) {
		if (GameManager->Map.Contains(Cell + FIntVector(0, -1, 0))) { // if there is a wall - try to bypass it from a side
			if (LocalPos.Y > CellSize.Y / 2) { // if we can go little bit more to center
				SecondaryShift.Y = -1;
			}
			else { // can't move to this direction any more
				if (LocalPos.X < CellSize.X / 2) {
					SecondaryShift.X = !GameManager->Map.Contains(Cell + FIntVector(-1, -1, 0)) && !GameManager->Map.Contains(Cell + FIntVector(-1, 0, 0)) ? -1 : 0;
				}
				else {
					SecondaryShift.X = !GameManager->Map.Contains(Cell + FIntVector(1, -1, 0)) && !GameManager->Map.Contains(Cell + FIntVector(1, 0, 0)) ? 1 : 0;
				}
			}
		}
		else { // no wall
			if (FMath::IsNearlyEqual(LocalPos.X, CellSize.X / 2, CellSize.X / 10)) { // if we on center of the cell
				Shift.Y = -1;
			}
			else { // need to be centred
				SecondaryShift.X = LocalPos.X < CellSize.X / 2 ? 1 : -1;
			}
		}
	}
	if (IsMovingLeft) {
		if (GameManager->Map.Contains(Cell + FIntVector(1, 0, 0))) { // if there is a wall - try to bypass it from a side
			if (LocalPos.X < CellSize.X / 2) { // if we can go little bit more to center
				SecondaryShift.X = 1;
			}
			else { // can't move to this direction any more
				if (LocalPos.Y < CellSize.Y / 2) {
					SecondaryShift.Y = !GameManager->Map.Contains(Cell + FIntVector(1, -1, 0)) && !GameManager->Map.Contains(Cell + FIntVector(0, -1, 0)) ? -1 : 0;
				}
				else {
					SecondaryShift.Y = !GameManager->Map.Contains(Cell + FIntVector(1, 1, 0)) && !GameManager->Map.Contains(Cell + FIntVector(0, 1, 0)) ? 1 : 0;
				}
			}
		}
		else { // no wall
			if (FMath::IsNearlyEqual(LocalPos.Y, CellSize.Y / 2, CellSize.Y / 10)) { // if we on center of the cell
				Shift.X = 1;
			}
			else { // need to be centred
				SecondaryShift.Y = LocalPos.Y < CellSize.Y / 2 ? 1 : -1;
			}
		}
	}
	if (IsMovingRight) {
		if (GameManager->Map.Contains(Cell + FIntVector(-1, 0, 0))) { // if there is a wall - try to bypass it from a side
			if (LocalPos.X > CellSize.X / 2) { // if we can go little bit more to center
				SecondaryShift.X = -1;
			}
			else { // can't move to this direction any more
				if (LocalPos.Y < CellSize.Y / 2) {
					SecondaryShift.Y = !GameManager->Map.Contains(Cell + FIntVector(-1, -1, 0)) && !GameManager->Map.Contains(Cell + FIntVector(0, -1, 0)) ? -1 : 0;
				}
				else {
					SecondaryShift.Y = !GameManager->Map.Contains(Cell + FIntVector(-1, 1, 0)) && !GameManager->Map.Contains(Cell + FIntVector(0, 1, 0)) ? 1 : 0;
				}
			}
		}
		else { // no wall
			if (FMath::IsNearlyEqual(LocalPos.Y, CellSize.Y / 2, CellSize.Y / 10)) { // if we on center of the cell
				Shift.X = -1;
			}
			else { // need to be centred
				SecondaryShift.Y = LocalPos.Y < CellSize.Y / 2 ? 1 : -1;
			}
		}
	}

	// Add secondary shifts
	if (FMath::IsNearlyZero(Shift.X)) {
		Shift.X += SecondaryShift.X;
	}
	if (FMath::IsNearlyZero(Shift.Y)) {
		Shift.Y += SecondaryShift.Y;
	}

	// Move if needed
	if (!Shift.IsNearlyZero()) {
		auto Direction = Shift;
		Direction.Normalize();
		OnFrontSideUpdate(Direction);

		Pos += Shift * CurrentSpeed * DeltaTime;
		SetActorLocation(Pos);

		auto OldCell = Cell;
		Cell = GameManager->ConvertPositionWorldToCell(Pos);
		if (OldCell != Cell) {
			// Did we step on a pickup?
			if (GameManager->Pickups.Contains(Cell)) {
				GameManager->Pickups[Cell]->Use(this);
			}

			// Generate chunks if needed
			auto CurrentChunk = GameManager->ConvertPositionCellToChunk(Cell);
			if (GameManager->ConvertPositionCellToChunk(OldCell) != CurrentChunk) {
				GameManager->GenerateChunksAround(CurrentChunk, GameManager->ViewRange);
			}

			UE_LOG(LogTemp, Log, TEXT("Player %d: Cell = %s"), PlayerId, *Cell.ToString());
		}
	}

	// Remote control buf fade
	if (HasBombRemote) {
		CurrentBombRemoteDuration -= DeltaTime;
		if (CurrentBombRemoteDuration <= 0) {
			CurrentBombRemoteDuration = 0;
			HasBombRemote = false;
			UpdateWidget();
		}
	}
}

// Called to bind functionality to input
void ABomberman::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABomberman::InitPlayer(int Id, FLinearColor Color)
{
	// General info
	PlayerId = Id;
	PlayerColor = Color;

	// Widget init
	GameManager->ScoreWidget->SetName(PlayerId, *(FString("Player ") + FString::FromInt(PlayerId + 1)));
	GameManager->ScoreWidget->SetColor(PlayerId, PlayerColor);
	UpdateWidget();

	// Init blueprint
	InitPlayerExternal();
}

int ABomberman::GetPlayerId()
{
	return PlayerId;
}

void ABomberman::MoveUp(bool Start)
{
	IsMovingUp = Start;
}

void ABomberman::MoveDown(bool Start)
{
	IsMovingDown = Start;
}

void ABomberman::MoveLeft(bool Start)
{
	IsMovingLeft = Start;
}

void ABomberman::MoveRight(bool Start)
{
	IsMovingRight = Start;
}

void ABomberman::PutBomb()
{
	if (BombCount <= 0 || RemoteBombPlaced || GameManager->Bombs.Contains(Cell)) {
		return;
	}
	BombCount--;
	GameManager->ScoreWidget->SetBombsCount(PlayerId, BombCount);

	auto Location = GameManager->ConvertPositionCellToWorld(Cell) + GameManager->CellSize / 2;
	auto Bomb = Cast<ABomb>(GetWorld()->SpawnActor(GameManager->BombClass, &Location));
	if (!Bomb) {
		UE_LOG(LogTemp, Error, TEXT("Cannot spawn bomb"));
		return;
	}
	Bomb->ExplosionRange = ExplosionRange;
	Bomb->BombOwner = this;
	Bomb->Cell = Cell;

	if (HasBombRemote) {
		Bomb->Type = EBombType::Remote;
		RemoteBombPlaced = true;
		CurrentBombRemoteDuration = BombRemoteDuration;
	}
	else {
		Bomb->Type = EBombType::Timer;
		Bomb->StartTimer();
	}

	Bombs.Add(Bomb);
	GameManager->Bombs.Add(Cell, Bomb);
}

void ABomberman::Trigger()
{
	for (auto Bomb : Bombs) {
		if (Bomb->Type == EBombType::Remote) {
			Bomb->Trigger();
			break;
		}
	}
}

void ABomberman::Die()
{
	GameManager->Players.Remove(this);
	Destroy();
}

void ABomberman::BombExploded(ABomb * Bomb)
{
	if (Bomb->Type == EBombType::Remote) {
		RemoteBombPlaced = false;
	}
	Bombs.Remove(Bomb);
	BombCount++;
	GameManager->Bombs.Remove(Bomb->Cell);
	GameManager->ScoreWidget->SetBombsCount(PlayerId, BombCount);
}

void ABomberman::UpdateWidget()
{
	GameManager->ScoreWidget->SetRemoteControl(PlayerId, HasBombRemote);
	GameManager->ScoreWidget->SetBombsCount(PlayerId, BombCount);
	GameManager->ScoreWidget->SetSpeed(PlayerId, NormalSpeed, CurrentSpeed);
	GameManager->ScoreWidget->SetScore(PlayerId, Score);
}

void ABomberman::InitPlayerExternal_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("InitPlayerExternal_Implementation called, please create blueprint implementation"));
}

