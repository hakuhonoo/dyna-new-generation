// Made by Arendariuk Andrii

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ScoreWidget.generated.h"

/**
 * 
 */
UCLASS()
class DYNA_API UScoreWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widget)
	void SetName(int PlayerId, FName Name);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widget)
	void SetColor(int PlayerId, FLinearColor Color);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widget)
	void SetSpeed(int PlayerId, float Normal, float Current);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widget)
	void SetBombsCount(int PlayerId, int Count);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widget)
	void SetRemoteControl(int PlayerId, bool Exists);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widget)
	void SetScore(int PlayerId, int Score);
};
