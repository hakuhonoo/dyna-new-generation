// Made by Arendariuk Andrii

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bomb.generated.h"

UENUM(BlueprintType)
enum class EBombType : uint8 {
	Timer, Remote
};

UCLASS()
class DYNA_API ABomb : public AActor
{
	GENERATED_BODY()
	
public:	
	ABomb();

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	virtual void Tick(float DeltaTime) override;

protected:
	class AGameManager* GameManager = nullptr;

public:
	TSharedPtr<int> ExplosionChainLength = nullptr;

public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Bomb)
	void Trigger();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Bomb)
	void StartTimer();

	UFUNCTION(BlueprintCallable, Category = Bomb)
	void DestroySurroundings();

	UFUNCTION(BlueprintCallable, Category = Bomb)
	void OnExploded();

public:
	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Bomb)
	FIntVector Cell;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Bomb)
	int ExplosionRange = 1;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Bomb)
	float BombTimer = 3;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Bomb)
	EBombType Type = EBombType::Timer;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = Bomb)
	class ABomberman* BombOwner;
};
