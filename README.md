## Dyna: Next generation

Remake of old DOS Bomberman-like game on UE4

---

Next step would be finishing AI of monster enemy, then AI of enemy player.
Also there is lack of main menu with few game modes like single player, PvP, Coop.
Well... And real multiplayer, LAN/Internet. Structure of project should be little bit different though, 
but it's always nice to have such things in the game!
And test/bug fix it all, of course :)

---

Not mine was only one particle system from UE4 Engine. I had modified it though.